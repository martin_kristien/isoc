# Compiler Optimisation Playground

This project aims at developing a tool for testing optimisation on immediate representation of a program.

The project is being developed as part of Informatics Summer of Code by Martin Kristien under the supervision of Hugh Leather.


# Building

Ant tool is used to build this project.

# Resources
### Ant Tutorial points
http://www.tutorialspoint.com/ant/ant_property_task.htm