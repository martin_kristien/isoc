package cop.regAlloc;

import cop.ir.iloc.ILOCProgram;

import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.HashMap;

public abstract class Allocate {
    protected Set<Integer>          all;
    protected Map<String, Integer>  special;

    /**
     * @param archFilename name of architecture file to be used to load number
     *        of physical registers
     */
    public Allocate(String archFilename) {
        // read architecture file to get size
    }

    /**
     * @param size    number of physical registers
     * @param special map of special architectural registers such as stack pointer, zero register
     */
    public Allocate(int size, Map<String, Integer> special) {
        initAll(size);
        this.special = special;
    }

    /**
     * Initialize {@code all} set with values from {@code 0} to {@code size-1} 
     * @param size number of all registers
     */
    private void initAll(int size) {
        all = new HashSet<Integer>(size, 1);
        for (int i = 0; i < size; i++) {
            all.add(new Integer(i));
        }
    }

   /**
    * This method modifies an ILOC program so that it only uses physical
    * registers defines in architecture
    * @param  prog        program to be allocated
    * @param  caller_save set of registers that are to be saved by caller
    *                     thus allocator can use them freely
    * @param  non_virtual set of registers that if appear in {@code prog}
    *                     correspond to physical registers and shall not be considered virtual
    * @return             program containing physical registers functionally equivalent to
    *                     {@code prog}
    */
    public abstract ILOCProgram allocate(
        ILOCProgram     prog,
        Set<Integer>    caller_save,
        Set<Integer>    non_virtual);
}