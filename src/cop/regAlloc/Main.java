package cop.regAlloc;

import cop.ir.iloc.*;

public class Main {
    public static void main(String [] args){
        // cop.ir.iloc.Demo.runCreatePrint("output/iloc/prog1.iloc");
        ILOCProgram prog = cop.ir.iloc.Demo.runLoadProg("output/iloc/prg1.iloc");
        int size = prog.size();
        System.out.println("loaded program has " + size + " instructions");
    }

    static void runReadAllocate() {
        ILOCProgram prog;
        ILOCReader reader;
        // ILOCRLCheck checker;

        prog = new ILOCProgram();
        reader = new ILOCReader("ilocprint");
        // checker = new ILOCRLCheck();

        prog.accept(reader);
        // Allocate allocator = new AllocateTopdown(2, 1);
        // prog = allocator.allocate(prog);
    }
}