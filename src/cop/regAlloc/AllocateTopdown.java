package cop.regAlloc;

import cop.ir.iloc.*;

import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.*;

/**
 * Implements Top-down register allocation.
 * <p>
 * Virtual registers are analysed to get occurence frequencies. The most frequent registers
 * are allocated physical registers. Given program is then rewritten to map allocated virtual registers
 * to physical registers. Unallocated registers must be loaded/stored from/to memory.
 * @author  Martin Kristien
 * @version 1.0
 * @since   2016-7-16
 */
public class AllocateTopdown extends Allocate {
    private HashMap<Integer, Integer>   frequencies = null;
    private HashMap<Integer, Integer>   mapping = null;
    private int                         size;
    private int                         f = 4;
    private Set<Integer>                caller_save;
    private Set<Integer>                non_virtual;
    private final String                MODULE = "[AllocateTopdown] ";

    /**
     * Constructor specifying number of physical registers explicitly
     * @param size number of physical registers
     * @param f number of temporary registers needed to load/store memory based registers
     */
    public AllocateTopdown(int size, Map<String, Integer> special) {
        super(size, special);
        this.size = all.size();
    }
    
    /**
     * Constructor passed name of an architecture file containing number of physical registers
     * @param archFilename path of architecture file to be used to load number
     *  of physical registers
     */
    public AllocateTopdown(String archFilename) {
        super(archFilename);
    }

    @Override
    public ILOCProgram allocate(
        ILOCProgram         prog,
        Set<Integer>    caller_save,
        Set<Integer>    non_virtual) {
        this.caller_save = caller_save;
        this.non_virtual = non_virtual;
        // check all member variables are initialized
        if (!allInitialized()) {
            System.out.println(MODULE + "allocate: some member variables were not initialized");
            return null;
        }

        // count register occurence frequencies
        loadFrequencies(prog);


        if (frequencies.size() < size) {
            // create mapping for all virtual registers
            System.out.println("size too small, no reduction needed");
            createMapping(frequencies.keySet());
        } else {
            // create mapping only for the most frequent virtual registers
            System.out.println("sorting virtual registers");
            List<Integer> virtualRegs = sortByValue(frequencies);
            System.out.println("sorted virtual registers: " + virtualRegs.toString());

            // get only size most frequent registers, create mappings for those
            List<Integer> frequentRegs = virtualRegs.subList(0, size - f);
            createMapping(new HashSet<Integer>(frequentRegs));
        }

        System.out.println("mapping: " + mapping.toString());

        // rewrite the code
        ILOCProgram prognew = rewriteProgram(prog);

        return prognew;
    }

    private boolean allInitialized() {
        return
            all != null         &&
            special != null     &&
            caller_save != null &&
            non_virtual != null;
    }

    /**
     * Generates a new ILOCProgram where all virtual registers are mapped to physical registers
     * @param prog source program to be rewritten
     * @return new physical registers only program equivalent to {@code prog}
     */
    private ILOCProgram rewriteProgram(ILOCProgram prog) {
        ILOCProgram prognew = new ILOCProgram();

        for (ILOCInstruction inst: prog.getInstructions()) {
            for (ILOCInstruction instnew: rewriteInst(inst)) {
                prognew.addInst(instnew);
            }
        }
        return prognew;
    }

    /**
     * Generates a list of equivalent instructions from a give instruction
     * @param inst virtual instruction to be rewritten into potentially several instructions
     * @return list of resultant instructions. Might containg code to load/store
     * unallocated virtual registers
     */
    private List<ILOCInstruction> rewriteInst(ILOCInstruction inst) {
        List<ILOCInstruction> insts = new LinkedList<ILOCInstruction>();

        for (ILOCOperand op: inst.getSourceList()) {
            if (op.getType() == ILOCTypes.ILOCOperandType.Register) {
                Integer physReg = mapping.get(op.getValue());
                if (physReg == null) {
                    // no mapping for this register, load from memory
                    System.out.println("mem load for reg: " + op.getValue());
                } else {
                    // physical register assign, pass mapping to result
                    System.out.println("mapping for reg: " + op.getValue() + " to: " + physReg);
                    // ILOCInstruction
                }
            }
        }
        for (ILOCOperand op: inst.getTargetList()) {
            if (op.getType() == ILOCTypes.ILOCOperandType.Register) {
                Integer physReg = mapping.get(op.getValue());
                if (physReg == null) {
                    // no mapping for this register, load from memory
                    System.out.println("mem store for reg: " + op.getValue());
                } else {
                    // physical register assign, pass mapping to result
                    System.out.println("mapping for reg: " + op.getValue() + " to: " + physReg);

                }
            }
        }
        return insts;
    }

    /**
     * Creates mapping from a set of virtual registers to physical registers
     * @param keys set of virtual registers
     */
    private void createMapping(Set<Integer> keys) {
        if (keys.size() > size) {
            System.out.println("error, keys set too large for mapping");
            return;
        }
        mapping = new HashMap<Integer, Integer>();
        Integer i = 0;
        for (Integer key: keys) {
            mapping.put(key, i);
            i++;
        }
    }

    /**
     * Sort keys in map by their associated values. Used for getting the most frequent
     * virtual registers
     * @param map key-value pairs to be sorted
     * @return list of keys from {@code map} in sorted order
     */
    private static <K, V extends Comparable<? super V>> List<K> sortByValue( Map<K, V> map ) {
        List<Map.Entry<K, V>> list = new LinkedList<Map.Entry<K, V>>( map.entrySet() );
        Collections.sort( list, new Comparator<Map.Entry<K, V>>() {
            public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 ) {
                return (o2.getValue()).compareTo(o1.getValue());
            }
        });
        // return list;
        List<K> result = new LinkedList<K>();
        for (Map.Entry<K, V> entry : list) {
            result.add(entry.getKey());
        }
        return result;
    }

    private void loadFrequencies(ILOCProgram prog) {
        frequencies = new HashMap<Integer, Integer>();

        for (ILOCInstruction inst: prog.getInstructions()) {
            loadFrequencies(inst);
        }
    }

    private void loadFrequencies(ILOCInstruction inst) {
        for (ILOCOperand operand: inst.getSourceList()) {
            loadFrequencies(operand);
        }
        for (ILOCOperand operand: inst.getTargetList()) {
            loadFrequencies(operand);
        }
    }

    private void loadFrequencies(ILOCOperand operand) {
        if (operand.getType() != ILOCTypes.ILOCOperandType.Register) {
            // operand is not a register, nothing to be done
            return;
        }

        Integer reg = (Integer) operand.getValue();
        addCount(reg);
    }

    private void addCount(Integer reg) {
        if (frequencies.containsKey(reg)) {
            frequencies.put(reg, frequencies.get(reg) + 1);
        } else {
            frequencies.put(reg, 1);
        }
    }
}