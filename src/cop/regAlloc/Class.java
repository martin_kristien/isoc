package cop.regAlloc;

// used by bottom-up register allocator

public class Class {
    protected int   size;
    public int      name[];
    public int      next[];
    public boolean  free[];
    int             stack[];
    int             stackTop;

    public Class(int size) {
        this.size = size;

        name = new int[size];
        initializeArray(name, -1);

        next = new int[size];
        initializeArray(next, Integer.MAX_VALUE);

        free = new boolean[size];
        initializeArray(free, true);

        stack = new int[size];
        for (int i = 0; i < size; i++) {
            stack[i] = i;
        }
        stackTop = size - 1;
    }

    private void initializeArray(int array[], int value) {
        for (int i=0; i < array.length; i++) {
            array[i] = value;
        }
    }

    private void initializeArray(boolean array[], boolean value) {
        for (int i=0; i < array.length; i++) {
            array[i] = value;
        }
    }

   /**
    *   Pushes value onto stack. Returns 0 if unsuccessfull.
    */
    public int push(int value) {
        if (stackTop == size -1) {
            // stack is full
            return 0;
        }

        stackTop++;
        stack[stackTop] = value;
        return 1;
    }

   /**
    *   Gets value from stack. Returns -1 if unsuccessfull
    */
    public int pull() {
        if (stackTop < 0) {
            // stack empty
            return -1;
        }

        int result = stack[stackTop];
        stackTop--;

        return result;
    }
}