package cop.ir.iloc;

import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

public class ILOCProgram implements ILOCVisitee {
	protected List<ILOCInstruction> instructions;
	private static final String MODULE = "[ILOCProgram] ";

	/**
	 * Creates new empty ILOCProgram
	 */
	public ILOCProgram() {
		instructions = new LinkedList<ILOCInstruction>();
	}

	/**
	 * Appends new ILOCInstruction at the end of this program
	 * @param inst instruction to be appended
	 */
	public void addInst(ILOCInstruction inst) {
		if (inst == null) {
			System.out.println(MODULE + "addInst: error, parameter is null");
		}
		instructions.add(inst);
	}

	/**
	 * Adds new ILOCInstruction at particular location in this program
	 * @param inst instruction to be added
	 * @param idx  location where {@code inst} shall be added
	 */
	public void addInst(ILOCInstruction inst, int idx) {
		instructions.add(idx, inst);
	}

	/**
	 * Replaces first occurence of a particular instruction with
	 * a list of instructions
	 * @param inst instruction to be replaced
	 * @param list list of instructions to replace {@code inst}
	 */
	public void replace(ILOCInstruction inst, List<ILOCInstruction> list) {
		int idx = instructions.indexOf(inst);
		if (idx == -1) {
			System.out.println(MODULE + "replace: input instructions does not occur in this program");
			return;
		}

		removeInst(idx);
		for (ILOCInstruction newinst: list) {
			addInst(newinst, idx);
			idx++;
		}
	}

	/**
	 * Inserts a list of instructions to particular place in this program
	 * @param list list of instructions to be inserted
	 * @param idx  place where first instruction from {@code list} shall be inserted
	 */
	public void insert(List<ILOCInstruction> list, int idx) {
		for (ILOCInstruction inst: list) {
			addInst(inst, idx);
			idx++;
		}
	}

	/**
	 * Returns ILOCInstruction at particular position in this program
	 * @param  idx position of instruction to be retrieved
	 * @return     instruction at {@code idx} in this program
	 */
	public ILOCInstruction getInst(int idx) {
		return instructions.get(idx);
	}

	/**
	 * Returns a list of all instructions in this program
	 * @return list of all instructions
	 */
	public List<ILOCInstruction> getInstructions() {
		return instructions;
	}

	/**
	 * Removes particular instruction from this program
	 * @param  idx location of instruction to be removed
	 * @return     removed instruction
	 */
	public ILOCInstruction removeInst(int idx) {
		return instructions.remove(idx);
	}

	/**
	 * Returns size of this program in terms of number of instructions
	 * @return size of this program
	 */
	public int size() {
		return instructions.size();
	}

	public <T> T accept(ILOCVisitor<T> v) {
		return v.visitProg(this);
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ILOCProgram)) {
			return false;
		}
		ILOCProgram prog = (ILOCProgram) obj;
		if (instructions.size() != prog.instructions.size()) {
			return false;
		}


		ILOCInstruction inst1, inst2;
		for (int i = 0; i< instructions.size(); i++) {
			inst1 = instructions.get(i);
			inst2 = prog.instructions.get(i);
			if (!inst1.equals(inst2)) {
				return false;
			}
		}
		return true;
	}
}