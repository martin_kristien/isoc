package cop.ir.iloc;

import java.util.LinkedList;
import java.util.List;
import cop.ir.iloc.ILOCTypes.ILOCOpcode;

/**
 * ILOCInstruction represents an instruction in ILOC program. An instruction can have
 * a label associated with it. All instructions have opcode and can have a list of source
 * and target operands.
 */
public class ILOCInstruction implements ILOCVisitee {
    private final static String MODULE = "[ILOCInstruction] ";
    protected String label;
    protected ILOCOpcode opcode;
    protected List<ILOCOperand> sourceList;
    protected List<ILOCOperand> targetList;
    /**
     * Empty instruction constructor. Creates NOP instruction.
     */
	public ILOCInstruction() {
        this(ILOCOpcode.nop);
	}
    /**
     * Creates an isntruction with a particular opcode
     * @param opcode opcode for the new instruction
     */
    public ILOCInstruction(ILOCOpcode opcode) {
        this.opcode = opcode;
        label = "";
        sourceList = new LinkedList<ILOCOperand>();
        targetList = new LinkedList<ILOCOperand>();
    }

    /**
     * Adds a source operand for this instruction to the end of the source operand list
     * @param operand operand to be added
     */
    public void addSource(ILOCOperand operand) {
        sourceList.add(operand);
    }

    /**
     * Adds a source operand for this instruction at a particular index in the source operand list
     * @param operand operand to be added
     * @param idx index where {@code operand} shall be added in the source operand list
     */
    public void addSource(ILOCOperand operand, int idx) {
        sourceList.add(idx, operand);
    }

    /**
     * Adds a target operand for this instruction to the end of the target operand list
     * @param operand operand to be added
     */
    public void addTarget(ILOCOperand operand) {
        targetList.add(operand);
    }

    /**
     * Adds a target operand for this instruction at a particular index in the target operand list
     * @param operand operand to be added
     * @param idx index where {@code operand} shall be added in the target operand list
     */
    public void addTarget(ILOCOperand operand, int idx) {
        targetList.add(operand);
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void setOpcode(ILOCOpcode opcode) {
        this.opcode = opcode;
    }

    public String getLabel() {
        return label;
    }

    public List<ILOCOperand> getSourceList() {
        return sourceList;
    }

    public List<ILOCOperand> getTargetList() {
        return targetList;
    }

    public ILOCOpcode getOpcode() {
        return opcode;
    }

	public <T> T accept(ILOCVisitor<T> v) {
		return v.visitInst(this);
	}

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            System.out.println(MODULE + "equals: parameter is null");
            return false;
        }

        if (!(obj instanceof ILOCInstruction)) {
            System.out.println(MODULE + "equals: not ILOCInstruction, "
                + obj.toString());
            return false;
        }
        ILOCInstruction inst = (ILOCInstruction) obj;

        return true;
    }
}