package cop.ir.iloc;

import java.io.*;
import cop.ir.iloc.ILOCTypes.ILOCOperandType;

public class ILOCPrinter implements ILOCVisitor<Void> {
    OutputStream out;
    PrintWriter writer;
    private final static String MODULE = "[ILOCPrinter] ";
    static final String filenameDefault = "ilocprint";
    String filename;

    /**
     * Contructor tries to open default file to be read
     */
    public ILOCPrinter() {
        this(filenameDefault);
    }
    /**
     * Constructor tries to open particular file to which any program will be printed
     * @param  filename path to file to be printed to
     */
    public ILOCPrinter(String filename) {
        this.filename = filename;
        try{
            out = new FileOutputStream(filename);
            writer = new PrintWriter(out);
        } catch (Exception e) {
            e.printStackTrace();
            writer = null;
        }
    }

    public void close() {
        writer.close();
    }

    /**
     * Prints ILOCProgram into a file specified in contructor
     * @param  prog program to be printed
     * @return      null pointer, as no return value is necessary
     */
    public Void visitProg(ILOCProgram prog) {
        System.out.println(MODULE + "visitProg: printing ILOCProgram to " + filename);
        writer.print("ILOCProgram ");
        writer.println(prog.instructions.size());
        for(ILOCInstruction inst : prog.instructions) {
            inst.accept(this);
        }
        writer.flush();
        System.out.println(MODULE + "visitProg: finished printing program");
        return null;
    }

    /**
     * Prints ILOCInstruction into a file specified in contructor
     * @param  inst instruction to be printed
     * @return      null pointer, as no return value is necessary
     */
    public Void visitInst(ILOCInstruction inst) {
        writer.print(inst.label);
        writer.print(":");
        writer.print(inst.opcode);
        writer.print(":");

        boolean first = true;
        for(ILOCOperand op: inst.sourceList) {
            if (!first) {
                writer.print(",");
            } else {
                first = false;
            }
            op.accept(this);
        }
        writer.print(":");
        first = true;
        for(ILOCOperand op: inst.targetList) {
            if (!first) {
                writer.print(",");
            } else {
                first = false;
            }
            op.accept(this);
        }
        writer.println("");
        return null;
    }

    /**
     * Prints ILOCOperand into a file specified in contructor
     * @param  op operand to be printed
     * @return    null pointer, as no return value is necessary
     */
    public Void visitOperand(ILOCOperand op) {
        switch (op.type) {
            case Register: {
                writer.print("R");
                writer.print(op.regValue);
                break;
            }
            case Label: {
                writer.print("L");
                writer.print(op.labelValue);
                break;
            }
            case Number: {
                writer.print("N");
                writer.print(op.numValue);
                break;
            }
            default: {
                System.out.println(MODULE + "visitOperand: unknown operand type");
                break;
            }
        }
        return null;
    }
}