package cop.ir.iloc;
import cop.ir.iloc.ILOCTypes.ILOCOperandType;
public class ILOCRLCheck implements ILOCVisitor<Integer> {
    Integer max;
    protected int errors;
    public ILOCRLCheck() {
        max = new Integer(0);
        errors = 0;
    }

    public Integer visitProg(ILOCProgram prog) {
        for (ILOCInstruction inst:prog.instructions) {
            inst.accept(this);
        }
        return max;
    }

    public Integer visitInst(ILOCInstruction inst) {
        for (ILOCOperand op:inst.sourceList) {
            op.accept(this);
        }

        for (ILOCOperand op: inst.targetList) {
            op.accept(this);
        }
        return null;
    }

    public Integer visitOperand(ILOCOperand op) {
        if (op.type != ILOCOperandType.Register) {
            return null;
        }

        if (op.regValue > max) {
            max = op.regValue;
        }
        if (op.regValue < 0) {
            ++errors;
        }
        return null;
    }
}