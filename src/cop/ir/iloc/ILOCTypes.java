package cop.ir.iloc;
public class ILOCTypes {
	public enum ILOCOpcode {
		nop,

		// arithmetics register
		add, addI,
		sub, subI, rsumI,
		mult,multI,
		div, divI, rdivI,

		// shift
		lshift,	lshiftI,
		rshift,	rshiftI,

		// logic
		and, andI,
		or,	orI,
		xor, xorI,

		// memory
		loadI, load, loadAI, loadAO,
		cload, cloadAI, cloadAO,
		store, storeAI, storeAO,
		cstore, cstoreAI, cstoreAO,

		// miscelaous
		i2i, c2c, c2i, i2c,

		// control flow
		jump, jumpI,
		cbr,
		tbl,
		cmp_LT,	cmp_LE,	cmp_EQ,	cmp_GE,	cmp_GT,	cmp_NE,	comp,
		cbr_LT,	cbr_LE,	cbr_EQ,	cbr_GE,	cbr_GT,	cbr_NE,
	}
	public enum ILOCOperandType {
	    Register,
	    Label,
	    Number
	}
}