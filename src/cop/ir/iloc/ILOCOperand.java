package cop.ir.iloc;

import cop.ir.iloc.ILOCTypes.ILOCOperandType;
import cop.Globals;

public class ILOCOperand implements ILOCVisitee{
	
	protected ILOCOperandType type;
	protected Integer numValue;
	protected String labelValue;
	protected Integer regValue;

	private static final String MODULE = "[ILOCOperand] ";
	public ILOCOperand() {
	}

	public ILOCOperand(ILOCOperandType type, String value) {
		setOperand(type, value);
	}

	public void setOperand(ILOCOperandType type, String value) {
		this.type = type;
		switch (type) {
			case Register: {
				try {
					regValue = new Integer(value);
				} catch (NumberFormatException e) {
					System.out.println(MODULE + "setOperand: error, input is not an integer");
					Globals.errors++;
				}
				break;
			}
			case Label: {
				labelValue = value;
				break;
			}
			case Number: {
				try {
					numValue = new Integer(value);
				} catch (NumberFormatException e) {
					System.out.println(MODULE + "setOperand: error, input is not an integer");
					Globals.errors++;
				}
				break;
			}
			default: {
				System.out.println("[ILOCOperand] unknown ILOCOperandType in constructor");
				Globals.errors++;
				break;
			}
		}
	}

	public ILOCOperandType getType() {
		return type;
	}

	public Object getValue() {
		switch (type) {
			case Register: 	return regValue;
			case Label:		return labelValue;
			case Number:	return numValue;
		}
		return null;
	}

	public <T> T accept(ILOCVisitor<T> v) {
		return v.visitOperand(this);
	}
}