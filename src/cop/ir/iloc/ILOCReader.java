package cop.ir.iloc;

import java.io.*;
import cop.ir.iloc.ILOCTypes.ILOCOperandType;
import cop.ir.iloc.ILOCTypes.ILOCOpcode;

public class ILOCReader implements ILOCVisitor<Void> {
    BufferedReader reader;
    String line;
    private final static String MODULE = "[ILOCReader] ";
    private static final String filenameDefault = "ilocprint";
    private String filename;

    // inter-method shared variables
    String value;

    public ILOCReader() {
        this(filenameDefault);
    }

    public ILOCReader(String filename) {
        this.filename = filename;
        try{
            reader = new BufferedReader(new FileReader(filename));
        } catch (Exception e) {
            // e.printStackTrace();
            System.out.println(MODULE + "cannot open file " + filename);
            reader = null;
        }
    }

    private void loadLine() {
        try{
            line = reader.readLine();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Void visitProg(ILOCProgram prog) {
        if (reader == null) {
            System.out.println(MODULE + "visitProg: reader is not initialized");
            return null;
        }
        loadLine();
        String [] lines = line.split(" ");
        if (!lines[0].equals("ILOCProgram")) {
            System.out.println(MODULE + "visitProg: input file is not an ILOCProgram");
        }
        int size = Integer.parseInt(lines[1]);

        ILOCInstruction inst;
        for(int i=0; i<size; i++) {
            // inst = new ILOCInstruction();
            loadLine();
            inst = ILOCFactory.parseInst(line);
            // visitInst(inst);
            prog.addInst(inst);
        }

        return null;
    }

    public Void visitInst(ILOCInstruction inst) {
        String [] section = line.split(":");
        inst.setLabel(section[0]);

        ILOCOpcode opcode = ILOCOpcode.valueOf(section[1]);
        inst.setOpcode(opcode);

        String [] operands;
        ILOCOperand op;

        operands = section[2].split(",");
        for (String operand:operands) {
            op = new ILOCOperand();
            value = operand;
            visitOperand(op);
            inst.addSource(op);
        }

        operands = section[3].split(",");
        for (String operand:operands) {
            op = new ILOCOperand();
            value = operand;
            visitOperand(op);
            inst.addTarget(op);
        }

        return null;
    }

    public Void visitOperand(ILOCOperand op) {
        char type = value.charAt(0);
        String v  = value.substring(1);
        switch (type) {
            case 'R': {
                op.setOperand(ILOCOperandType.Register, v);
                break;
            }
            case 'L': {
                op.setOperand(ILOCOperandType.Label, v);
                break;
            }
            case 'N': {
                op.setOperand(ILOCOperandType.Number, v);
                break;
            }
        }
        return null;
    }
}