package cop.ir.iloc;

public interface ILOCVisitee {
	<T> T accept(ILOCVisitor<T> v);
}