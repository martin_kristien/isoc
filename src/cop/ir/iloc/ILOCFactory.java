package cop.ir.iloc;

import cop.ir.iloc.ILOCTypes.ILOCOperandType;
import cop.ir.iloc.ILOCTypes.ILOCOpcode;
import cop.Globals;
public class ILOCFactory {
	public static int errors = 0;
	private static final String MODULE = "[ILOCFactory] ";
	/**
	 * This method constructs an ILOCInstruction from a String representation
	 * @param  line String to be parsed
	 * @return      resultant ILOCInstruction
	 */
	public static ILOCInstruction parseInst(String line) {
		String [] section = line.split(":", -1);

		// check string is split into 4 parts
		if (section.length != 4) {
			errors++;
			System.out.println(MODULE + "parseInst: error, line not in 4 parts" + 
				" but " + section.length + ": " + line);
			return null;
		}

		ILOCInstruction inst = new ILOCInstruction();
		inst.setLabel(section[0]);
		ILOCOpcode opcode;
		try {
			opcode = ILOCOpcode.valueOf(section[1]);
		} catch (IllegalArgumentException e) {
			errors++;
			System.out.println(MODULE + "parseInst: error, bad opcode");
			return null;
		}
        inst.setOpcode(opcode);
		String [] operands;
        ILOCOperand op;

        operands = section[2].split(",");
        for (String operand:operands) {
            op = parseOperand(operand);
            inst.addSource(op);
        }
        operands = section[3].split(",");
        for (String operand:operands) {
            op = parseOperand(operand);
            inst.addTarget(op);
        }
        return inst;
	}

	/**
	 * This method constructs an ILOCOperand from a String representation
	 * @param  value String to be parsed
	 * @return       resultant ILOCOperand
	 */
	public static ILOCOperand parseOperand(String value) {
		ILOCOperand op = new ILOCOperand();

		// check string has at least 2 characters
		if (value.length() < 2) {
			errors++;
			System.out.println(MODULE + "parseOperand: error, input too short");
			return null;
		}

		char type = value.charAt(0);
		type = Character.toUpperCase(type);
		String v  = value.substring(1);
		Globals.resetErrors();
		switch (type) {
			case 'R': {
				op.setOperand(ILOCOperandType.Register, v);
				break;
			}
			case 'L': {
				op.setOperand(ILOCOperandType.Label, v);
				break;
			}
			case 'N': {
				op.setOperand(ILOCOperandType.Number, v);
				break;
			}
			default: {
				errors++;
				System.out.println(MODULE + "parseOperand: error, unrecognized type");
				return null;
			}
		}

		// check no error occured in setOperand
		if (Globals.errors > 0) {
			errors++;
			System.out.println(MODULE + "parseOperand: error, in setOperand");
			return null;
		}
		return op;
	}
}