package cop.ir.iloc;

public interface ILOCVisitor<T> {
	T visitInst(ILOCInstruction inst);
	T visitProg(ILOCProgram prog);
    T visitOperand(ILOCOperand op);
}