package cop.ir.iloc;

import cop.ir.iloc.ILOCTypes.ILOCOpcode;
import cop.ir.iloc.ILOCTypes.ILOCOperandType;
import cop.regAlloc.*;

public class Demo {
    public static void runReadCheckRL(){
        ILOCProgram prog;
        ILOCReader reader;
        ILOCRLCheck checker;

        prog = new ILOCProgram();
        reader = new ILOCReader("ilocprint");
        checker = new ILOCRLCheck();

        prog.accept(reader);
        Integer max = prog.accept(checker);

        if (max>=32) {
            System.out.println("[Demo runReadCheckRL] too many registers, largest "+max);
        } else {
            System.out.println("[Demo runReadCheckRL] # of regs OK");
        }

        if (checker.errors != 0) {
            System.out.println("[Demo runReadCheckRL] negavite registers present");
        }
    }

    public static void runCreatePrintReadPrint() {
        ILOCProgram prog;
        ILOCInstruction inst;
        ILOCOperand operand;

        prog = new ILOCProgram();

        // storeAI r_0 -> r_1 , 4
        inst = new ILOCInstruction(ILOCOpcode.subI);
        operand = new ILOCOperand(ILOCOperandType.Register, "0");
        inst.addSource(operand);

        operand = new ILOCOperand(ILOCOperandType.Register, "1");
        inst.addTarger(operand);

        operand = new ILOCOperand(ILOCOperandType.Number, "4");
        inst.addTarger(operand);
        prog.addInst(inst);

        // add r_0, r_1 -> r_1
        inst = new ILOCInstruction(ILOCOpcode.add);
        operand = new ILOCOperand(ILOCOperandType.Register, "0");
        inst.addSource(operand);

        operand = new ILOCOperand(ILOCOperandType.Register, "1");
        inst.addSource(operand);

        operand = new ILOCOperand(ILOCOperandType.Register, "1");
        inst.addTarger(operand);
        prog.addInst(inst);

        ILOCVisitor<Void> printer = new ILOCPrinter();
        prog.accept(printer);

        prog = new ILOCProgram();
        ILOCReader reader = new ILOCReader("ilocprint");
        prog.accept(reader);

        printer = new ILOCPrinter("ilocafter");
        prog.accept(printer);
    }

    public static void runReadModifyPrint() {
        ILOCProgram prog = new ILOCProgram();
        ILOCReader reader = new ILOCReader("ilocprint");
        prog.accept(reader);

        /* program is loaded */
        if (prog.instructions.size()>0) {
            System.out.println("[Demo runReadModifyPrint] duplicating first inst");
            prog.addInst(prog.getInst(0));
        }

        ILOCPrinter printer = new ILOCPrinter("ilocafter");
        prog.accept(printer);
    }

    public static void runCreatePrint(String filename) {
        ILOCProgram prog;
        ILOCInstruction inst;
        ILOCOperand operand;
        prog = new ILOCProgram();

        // storeAI r_0 -> r_1 , 4
        {
            inst = new ILOCInstruction(ILOCOpcode.subI);
            operand = new ILOCOperand(ILOCOperandType.Register, "0");
            inst.addSource(operand);

            operand = new ILOCOperand(ILOCOperandType.Register, "1");
            inst.addTarger(operand);

            operand = new ILOCOperand(ILOCOperandType.Number, "4");
            inst.addTarger(operand);
            prog.addInst(inst);
        }

        // add r_0, r_1 -> r_1
        {
            inst = new ILOCInstruction(ILOCOpcode.add);
            operand = new ILOCOperand(ILOCOperandType.Register, "0");
            inst.addSource(operand);

            operand = new ILOCOperand(ILOCOperandType.Register, "1");
            inst.addSource(operand);

            operand = new ILOCOperand(ILOCOperandType.Register, "1");
            inst.addTarger(operand);
            prog.addInst(inst);
        }

        ILOCVisitor<Void> printer = new ILOCPrinter(filename);
        prog.accept(printer);
    }

    public static ILOCProgram runLoadProg(String filename) {
        ILOCProgram prog = new ILOCProgram();
        ILOCReader  reader = new ILOCReader(filename);
        prog.accept(reader);

        return prog;
    }
}

