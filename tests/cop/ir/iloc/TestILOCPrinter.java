package cop.ir.iloc;
import org.junit.Test;
import junit.framework.*;

import java.io.*;
import cop.ir.iloc.*;
public class TestILOCPrinter extends TestCase {

	String inst1 = "hello:subI:R2,R5:Lhi,N-6";
	String inst2 = ":addI:Lhello,N5:R13";
	String inst3 = "hi:add:R2,R3:R4";

	@Test
	public void testPrinter() {
		String filename = "temp_test_printer";

		ILOCInstruction inst;
		ILOCProgram prog = new ILOCProgram();
		prog.addInst(ILOCFactory.parseInst(inst1));
		prog.addInst(ILOCFactory.parseInst(inst2));
		prog.addInst(ILOCFactory.parseInst(inst3));

		ILOCPrinter printer = new ILOCPrinter(filename);
		prog.accept(printer);
		printer.close();

		assertEquals(true, checkContent(filename));

		ILOCReader reader = new ILOCReader(filename);
		ILOCProgram prog2 = new ILOCProgram();
		prog2.accept(reader);

		assertEquals(prog, prog2);
	}

	private boolean checkContent(String filename) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch(Exception e) {
			System.out.println("checkContent: error, cannot open file " + filename);
		}

		if (reader == null) {
			return false;
		}

		String line = getLine(reader);
		assertEquals("ILOCProgram 3", line);

		line = getLine(reader);
		assertEquals(inst1, line);

		line = getLine(reader);
		assertEquals(inst2, line);

		line = getLine(reader);
		assertEquals(inst3, line);
		return true;
	}

	private String getLine(BufferedReader reader) {
		String line = "";
		try {
			line = reader.readLine();
		} catch (Exception e) {
			line = "";
		}
		return line;
	}
}