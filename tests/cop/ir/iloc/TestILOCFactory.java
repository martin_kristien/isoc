package cop.ir.iloc;
import org.junit.Test;
// import static org.junit.Assert.assertEquals;
import junit.framework.*;

// import cop.ir.iloc.*;
import cop.ir.iloc.ILOCTypes.*;
import java.util.*;
public class TestILOCFactory extends TestCase {

	@Test
	public void testParseOperandTooShort() {
		ILOCOperand operand;

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("r");
		assertEquals(null, operand);
		assertEquals(true, ILOCFactory.errors>0);
	}

	@Test
	public void testParseOperandBadLabel() {
		ILOCOperand operand;

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("w2");
		assertEquals(null, operand);
		assertEquals(true, ILOCFactory.errors>0);
	}

	@Test
	public void testParseOperandBadInteger() {
		ILOCOperand operand;

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("r254d");
		assertEquals(null, operand);
		assertEquals(true, ILOCFactory.errors>0);

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("n254d");
		assertEquals(null, operand);
		assertEquals(true, ILOCFactory.errors>0);
	}

	@Test
	public void testParseOperandRegister() {
		ILOCOperand operand;

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("r24");
		assertEquals(true, ILOCFactory.errors == 0);
		assertEquals(new Integer(24), (Integer) operand.getValue());
		assertEquals(ILOCTypes.ILOCOperandType.Register, operand.getType());
	}

	@Test
	public void testParseOperandNumber() {
		ILOCOperand operand;

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("N-50");
		assertEquals(true, ILOCFactory.errors == 0);
		assertEquals(new Integer(-50), (Integer) operand.getValue());
		assertEquals(ILOCTypes.ILOCOperandType.Number, operand.getType());
	}

	@Test
	public void testParseOperandLabel() {
		ILOCOperand operand;

		ILOCFactory.errors = 0;
		operand = ILOCFactory.parseOperand("lhello");
		assertEquals(true, ILOCFactory.errors == 0);
		assertEquals("hello", (String) operand.getValue());
		assertEquals(ILOCTypes.ILOCOperandType.Label, operand.getType());
	}
		@Test
	public void testParseInstBadFormat() {
		ILOCInstruction inst;

		ILOCFactory.errors = 0;
		inst = ILOCFactory.parseInst("::");
		assertEquals(null, inst);
		assertEquals(true, ILOCFactory.errors > 0);

		ILOCFactory.errors = 0;
		inst = ILOCFactory.parseInst("::::");
		assertEquals(null, inst);
		assertEquals(true, ILOCFactory.errors > 0);
	}

	@Test
	public void testParseInstBadOpcode() {
		ILOCInstruction inst;

		ILOCFactory.errors = 0;
		inst = ILOCFactory.parseInst(":bad_opcode::");
		assertEquals(null, inst);
		assertEquals(true, ILOCFactory.errors > 0);
	}

	@Test
	public void testParseInstFullExample() {
		ILOCInstruction inst;

		ILOCFactory.errors = 0;
		inst = ILOCFactory.parseInst("hello:subI:r54,lhello,n-5:n10,r5,lbye");

		assertEquals("hello", inst.getLabel());
		assertEquals(ILOCTypes.ILOCOpcode.subI, inst.getOpcode());

		List<ILOCOperand> list;
		ILOCOperand operand;

		list = inst.getSourceList();
		assertNotNull(list);
		assertEquals(3, list.size());

		operand = list.get(0);
		assertEquals(ILOCTypes.ILOCOperandType.Register, operand.getType());
		assertEquals(new Integer(54), operand.getValue());

		operand = list.get(1);
		assertEquals(ILOCTypes.ILOCOperandType.Label, operand.getType());
		assertEquals("hello", operand.getValue());

		operand = list.get(2);
		assertEquals(ILOCTypes.ILOCOperandType.Number, operand.getType());
		assertEquals(new Integer(-5), operand.getValue());

		list = inst.getTargetList();
		assertNotNull(list);
		assertEquals(3, list.size());

		operand = list.get(0);
		assertEquals(ILOCTypes.ILOCOperandType.Number, operand.getType());
		assertEquals(new Integer(10), operand.getValue());

		operand = list.get(1);
		assertEquals(ILOCTypes.ILOCOperandType.Register, operand.getType());
		assertEquals(new Integer(5), operand.getValue());

		operand = list.get(2);
		assertEquals(ILOCTypes.ILOCOperandType.Label, operand.getType());
		assertEquals("bye", operand.getValue());
	}
}