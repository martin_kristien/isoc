package cop.ir.iloc;
import org.junit.Test;
import junit.framework.*;

import cop.ir.iloc.*;
import cop.ir.iloc.ILOCTypes.*;
public class TestILOCProgram extends TestCase {
	ILOCProgram prog;

	public void testInitSize() {
		ILOCProgram prog = new ILOCProgram();
		int size = prog.size();
		assertEquals(size, 0);
	}

	public void testInitSetup() {
		assertEquals(prog.size(), 2);

		ILOCInstruction inst;
		ILOCOpcode opcode;
		// test first instruction
		inst = prog.getInst(0);
		assertEquals(inst.getOpcode(), ILOCOpcode.subI);


		// test second instruction
		inst = prog.getInst(1);
		opcode = inst.getOpcode();
		assertEquals(opcode, ILOCOpcode.add);
	}

	protected void setUp() {
		ILOCInstruction inst;
		ILOCOperand operand;
		prog = new ILOCProgram();

		// storeAI r_0 -> r_1 , 4
		{
			inst = new ILOCInstruction(ILOCOpcode.subI);
			operand = new ILOCOperand(ILOCOperandType.Register, "0");
			inst.addSource(operand);

			operand = new ILOCOperand(ILOCOperandType.Register, "1");
			inst.addTarget(operand);

			operand = new ILOCOperand(ILOCOperandType.Number, "4");
			inst.addTarget(operand);
			prog.addInst(inst);
		}

		// add r_0, r_1 -> r_1
		{
			inst = new ILOCInstruction(ILOCOpcode.add);
			operand = new ILOCOperand(ILOCOperandType.Register, "0");
			inst.addSource(operand);

			operand = new ILOCOperand(ILOCOperandType.Register, "1");
			inst.addSource(operand);

			operand = new ILOCOperand(ILOCOperandType.Register, "1");
			inst.addTarget(operand);
			prog.addInst(inst);
		}
   	}
}
